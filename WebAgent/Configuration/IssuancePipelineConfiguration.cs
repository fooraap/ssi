﻿namespace WebAgent.Configuration
{
    public class IssuancePipelineConfiguration
    {

        public const string SectionName = "IssuancePipelineConfiguration";

        public string SchemaDefinition { get; set; }

    }
}
