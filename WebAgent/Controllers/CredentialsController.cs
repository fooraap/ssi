using Flurl.Http;
using Hyperledger.Aries.Agents;
using Hyperledger.Aries.Configuration;
using Hyperledger.Aries.Extensions;
using Hyperledger.Aries.Features.DidExchange;
using Hyperledger.Aries.Features.IssueCredential;
using Hyperledger.Aries.Models.Records;
using Hyperledger.Aries.Storage;
using Hyperledger.Indy.DidApi;
using Hyperledger.Indy.LedgerApi;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebAgent.Models;
using WebAgent.Utils;
using WebAgent.Utils.credpipe;
using WebAgent.Utils.oidc;

namespace WebAgent.Controllers
{
    public class CredentialsController : Controller
    {
        private readonly IAgentProvider _agentContextProvider;
        private readonly ICredentialService _credentialService;

        private readonly IProvisioningService _provisionService;
        private readonly IWalletRecordService _recordService;
        private readonly IConnectionService _connectionService;
        private readonly ISchemaService _schemaService;
        private readonly IMessageService _messageService;

        private readonly OidcService _oidc;
        private readonly CredentialIssuancePipelineManager _pipelineManager;

        private readonly AgentConfiguration _options;
        private readonly ILogger _log;

        public CredentialsController(
            IAgentProvider agentContextProvider,
            ICredentialService credentialService,
            IProvisioningService provisionService,
            IWalletRecordService recordService,
            IConnectionService connectionService,
            ISchemaService schemaService,
            IMessageService messageService,
            CredentialIssuancePipelineManager pipelineManager,
            OidcService oidc,
            IOptions<AgentConfiguration> options, 
            ILogger<CredentialsController> log)
        {
            _agentContextProvider = agentContextProvider;
            _credentialService = credentialService;

            _provisionService = provisionService;
            _recordService = recordService;
            _connectionService = connectionService;
            _schemaService = schemaService;
            _messageService = messageService;

            _options = options.Value;
            _oidc = oidc;
            _pipelineManager = pipelineManager;
            _log = log;
        }


        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var context = await _agentContextProvider.GetContextAsync();
            var credentials = await _credentialService.ListAsync(context);
            var models = new List<CredentialViewModel>();
            foreach (var c in credentials)
            {
                models.Add(new CredentialViewModel
                {
                    SchemaId = c.SchemaId,
                    CreatedAt = c.CreatedAtUtc ?? DateTime.MinValue,
                    State = c.State,
                    CredentialAttributesValues = c.CredentialAttributesValues
                });
            }
            return View(new CredentialsViewModel { Credentials = models });
        }

        public async Task<IActionResult> InitiateEidCred(string id)
        {
            var newPipe = await _pipelineManager.CreateFromDefinitionIdAsync("D5HRDGDSppwraBqRgSgEzb:3:CL:6540:be_eid"); // TODO: move to config.
            var context = await _agentContextProvider.GetContextAsync();

            newPipe.Connection = await _recordService.GetAsync<ConnectionRecord>(context.Wallet, id);
            newPipe.Key = Guid.Parse(id);
            await _pipelineManager.StoreAsync(newPipe);
            return Redirect(await _oidc.Initiate(id));
        }

        [HttpGet]
        public async Task<IActionResult> ProcessEidCred([FromQuery] string code, [FromQuery] string state, [FromQuery] string customState)
        {
            var (profile, session, principal) = await _oidc.ProcessAndGetUser<EidProfile>(code, state);

            //profile.national_number = principal.

            profile = principal.Map<EidProfile>(profile);

            var pipeLine = await _pipelineManager.RestoreAsync(session.CustomState);

            if (pipeLine == null)
                return BadRequest();

            pipeLine.CredentialSourceData = profile;

            await _pipelineManager.IssueCredentialAsync(pipeLine);
            await _pipelineManager.ForgetAsync(pipeLine);
            return View("Success"); //Ok();
        }

        [HttpGet]
        public async Task<IActionResult> RegisterSchema()
        {
            var context = await _agentContextProvider.GetContextAsync();
            var issuer = await _provisionService.GetProvisioningAsync(context.Wallet);
            var Trustee = await Did.CreateAndStoreMyDidAsync(context.Wallet,
                new { seed = "ITKLYvSSxANOxF0ywqvawZ6caNbInlTV" }.ToJson());  //  000000000000000000000000Steward1
            await Ledger.SignAndSubmitRequestAsync(await context.Pool, context.Wallet, Trustee.Did,
                await Ledger.BuildNymRequestAsync(Trustee.Did, issuer.IssuerDid, issuer.IssuerVerkey, null, "ENDORSER"));

            var schemaId = await _schemaService.CreateSchemaAsync(
                context: context,
                issuerDid: issuer.IssuerDid,
                name: "eid-schema",
                version: _options.CredentialDefinitionVersion,
                attributeNames: new[] {
                    "place_of_birth_locality",
                    "beid_card_number",
                    "birthdate",
                    "gender",
                    "name",
                    "given_name",
                    "middle_name",
                    "family_name",
                    "age",
                    "address_street_address",
                    "address_locality",
                    "address_postal_code",
                    "national_number",
                    "timestamp",
                    "cert",
                    "document_type",
                    "nationality",
                    "chip_number",
                    "card_validity_end",
                    "card_validity_begin",
                    "card_delivery_municipality",
                });
            var newCdId = await _schemaService.CreateCredentialDefinitionAsync(context, new CredentialDefinitionConfiguration
            {
                SchemaId = schemaId,
                Tag = "be_eid",
                EnableRevocation = false,
                RevocationRegistrySize = 0,
                RevocationRegistryBaseUri = "",
                RevocationRegistryAutoScale = false,
                IssuerDid = issuer.IssuerDid
            });

            var newCredDef = await _recordService.GetAsync<DefinitionRecord>(context.Wallet, newCdId);

            _log.LogInformation("Created credential definition [{0}] for schema [{1}]", newCredDef.Id, newCredDef.SchemaId);

            return RedirectToAction("CredentialsForm");
        }

        [HttpGet]
        public async Task<IActionResult> CredentialsForm()
        {
            var context = await _agentContextProvider.GetContextAsync();
            var model = new CredentialFormModel
            {
                Connections = await _connectionService.ListAsync(context),
                CredentialDefinitions = await _schemaService.ListCredentialDefinitionsAsync(context.Wallet),
                Schemas = await _schemaService.ListSchemasAsync(context.Wallet)
            };
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> IssueCredentials(CredentialOfferModel model)
        {
            var context = await _agentContextProvider.GetContextAsync();
            var issuer = await _provisionService.GetProvisioningAsync(context.Wallet);
            var connection = await _connectionService.GetAsync(context, model.ConnectionId);
            var values = JsonConvert.DeserializeObject<List<CredentialPreviewAttribute>>(model.CredentialAttributes);

            foreach (CredentialPreviewAttribute attr in values)
            {
                attr.MimeType = CredentialMimeTypes.ApplicationJsonMimeType;
            }

            var (offer, _) = await _credentialService.CreateOfferAsync(context, new OfferConfiguration
            {
                CredentialDefinitionId = model.CredentialDefinitionId,
                IssuerDid = issuer.IssuerDid,
                CredentialAttributeValues = values
            });
            await _messageService.SendAsync(context, offer, connection);

            return RedirectToAction("Index");
        }

    }
}
