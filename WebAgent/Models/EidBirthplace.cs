﻿using WebAgent.Utils;

namespace WebAgent.Models
{
    public class EidBirthplace
    {
        [FromClaim()]
        public string locality { get; set; }

    }
}
