﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace WebAgent.Models
{
    public class PrimaryValue
    {
        [JsonProperty("n")]
        public string N { get; set; }

        [JsonProperty("s")]
        public string S { get; set; }

        [JsonProperty("r")]
        public Dictionary<string, string> Attributes { get; set; }

    }
}
