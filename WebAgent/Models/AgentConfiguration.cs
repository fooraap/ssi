﻿using System;
using System.Text;

namespace WebAgent.Models
{
    public class AgentConfiguration
    {

        public const string SectionName = "AgentConfiguration";


        public string SeedKey { get; set; }

        public string Name { get; set; }

        public string EndpointUrl { get; set; }

        public string WalletConfiguration { get; set; }

        public string WalletKey { get; set; }

        public string WalletPath { get; set; }

        public string GenesisUri { get; set; }

        public string PoolName { get; set; }

        public string LocalGenesisFile { get; set; }

        public string CredentialDefinitionVersion { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder("AGENT CONFIGURATION").Append(Environment.NewLine)
                                                             .Append("===========================================")
                                                             .Append(Environment.NewLine);
            foreach (var property in this.GetType().GetProperties())
                sb = sb.Append($"{property.Name} => {property.GetValue(this)}{Environment.NewLine}");
            return sb.ToString();
        }

    }
}
