﻿using Newtonsoft.Json;

namespace WebAgent.Models
{
    public class CredentialDefinition
    {

        [JsonProperty("ver")]
        public string Version { get; set; }

        [JsonProperty("id")]
        public string DefinitionId { get; set; }

        [JsonProperty("schemaId")]
        public string SchemaId { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("tag")]
        public string Tag { get; set; }

        [JsonProperty("value")]
        public DefinitionValue Value { get; set; }

    }
}
