﻿using Newtonsoft.Json;
using WebAgent.Utils;

namespace WebAgent.Models
{
    public class SchemaDefinition
    {
        [JsonProperty("version")]
        public string Version { get; set; }

        [JsonProperty("attr_names")]
        public string[] AttributeNames { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("id")]
        public string SchemaId { get; set; }

        [JsonProperty("seqNo")]
        public int SequenceNumber { get; set; }

        [JsonIgnore]
        public string Issuer => SchemaId.GetSplitPart(':', 0);

    }
}
