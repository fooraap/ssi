﻿using Newtonsoft.Json;

namespace WebAgent.Models
{
    public class DefinitionValue
    {

        [JsonProperty("primary")]
        public PrimaryValue Primary { get; set; }

        [JsonProperty("rctxt")]
        public string RCTXT { get; set; }

        [JsonProperty("z")]
        public string Z { get; set; }

    }
}
