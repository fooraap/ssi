﻿using WebAgent.Utils;

namespace WebAgent.Models
{
    public class EidAddress
    {
        [FromClaim()]
        public string street_address { get; set; }

        [FromClaim()]
        public string locality { get; set; }

        [FromClaim()]
        public string postal_code { get; set; }

    }
}
