﻿using System;
using WebAgent.Utils;

namespace WebAgent.Models
{
    public class EidProfile
    {

        [ExcludeNameValueEntry]
        [FromClaim(Recursive = true)]
        public EidBirthplace place_of_birth { get; set; }

        [FromClaim()]
        public string beid_card_number { get; set; }

        [FromClaim()]
        public string birthdate { get; set; }

        [FromClaim("sub")]
        public string national_number { get; set; }

        [FromClaim()]
        public string gender { get; set; }

        [FromClaim()]
        public string name { get; set; }

        [FromClaim()]
        public string given_name { get; set; }

        [FromClaim()]
        public string middle_name { get; set; }

        [FromClaim()]
        public string family_name { get; set; }

        [FromClaim()]
        public string age { get; set; }

        [FromClaim()]
        public string cert { get; set; }

        [FromClaim("beid_document_type")]
        public string document_type { get; set; }

        [FromClaim("beid_nationality")]
        public string nationality { get; set; }

        [FromClaim("beid_chip_number")]
        public string chip_number { get; set; }

        [FromClaim("beid_card_validity_begin")]
        public string card_validity_begin { get; set; }

        [FromClaim("beid_card_validity_end")]
        public string card_validity_end { get; set; }

        [FromClaim("beid_card_delivery_municipality")]
        public string card_delivery_municipality { get; set; }

        [ExcludeNameValueEntry]
        [FromClaim(Recursive = true)]
        public EidAddress address { get; set; }

        // Production values and VC attribute remapping 

        public string place_of_birth_locality => place_of_birth?.locality;

        public string address_street_address => address?.street_address;

        public string address_locality => address?.locality;

        public string address_postal_code => address?.postal_code;

        public string timestamp => DateTime.Now.Ticks.ToString();

    }
}
