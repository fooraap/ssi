﻿using System;

namespace WebAgent.Utils
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class FromClaimAttribute : Attribute
    {

        public FromClaimAttribute()
        {

        }

        public FromClaimAttribute(string claim)
        {
            Claim = claim;
        }

        public string Claim { get; set; }

        public bool Recursive { get; set; } = false;

    }
}
