﻿using System.Threading.Tasks;

namespace WebAgent.Utils.credpipe
{
    public interface IEntityEncoder<T> where T: class
    {

        Task<T> DecodeAsync(byte[] source);

        Task<byte[]> EncodeAsync(T source);

    }
}
