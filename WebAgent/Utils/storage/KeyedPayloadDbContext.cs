﻿using Microsoft.EntityFrameworkCore;

namespace WebAgent.Utils.credpipe
{
    public class KeyedPayloadDbContext<TItem, TKey> : DbContext where TItem: class, IKeyedItem<TKey>
    {


        //public CredentialIssuanceDataContext() :
        //     base(new DbContextOptionsBuilder<CredentialIssuanceDataContext>().UseNpgsql("User Id=foo; password=bar; Host=localhost; Database=foobar").Options)
        //{

        //}

        public KeyedPayloadDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<TItem> Items { get; set; }

    }
}
