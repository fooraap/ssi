﻿using Newtonsoft.Json;
using System.Text;
using System.Threading.Tasks;

namespace WebAgent.Utils.credpipe
{
    public class EntityJsonEncoder<T> : IEntityEncoder<T> where T: class
    {
        public Task<T> DecodeAsync(byte[] source)
        {
            if (source == null)
                return null;

            return Task.FromResult(JsonConvert.DeserializeObject<T>(Encoding.Default.GetString(source)));
        }

        public Task<byte[]> EncodeAsync(T source)
        {
            return Task.FromResult(Encoding.Default.GetBytes(JsonConvert.SerializeObject(source)));
        }
    }
}
