﻿using System.ComponentModel.DataAnnotations;

namespace WebAgent.Utils.credpipe
{
    public class KeyedPayload<TKey> : IKeyedItem<TKey>
    {

        [Key]
        public TKey Key { get; set; }

        public byte[] Payload { get; set; }

    }
}
