﻿using System.Threading.Tasks;

namespace WebAgent.Utils.credpipe
{
    public interface IStore<TItem, TKey> where TItem: IKeyedItem<TKey>
    {

        Task StoreAsync(TItem item);

        Task<TItem> LoadAsync(TKey key);

        Task RemoveAsync(TKey key);

    }
}
