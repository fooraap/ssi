﻿namespace WebAgent.Utils.credpipe
{
    public interface IKeyedItem<TKey>
    {

        TKey Key { get; }

    }
}
