﻿using System;

namespace WebAgent.Utils
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class ExcludeNameValueEntryAttribute : Attribute
    {
    }
}
