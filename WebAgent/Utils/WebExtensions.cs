﻿using Flurl.Http;
using System;
using System.IO;
using System.Threading.Tasks;

namespace WebAgent.Utils
{
    public static class WebExtensions
    {

        public static async Task<string> LeechAndStoreAsync(string uri, string backupLocalPath = null)
        {
            if (string.IsNullOrEmpty(uri))
                return null;

            var fileName = Path.GetTempFileName();

            try
            {
                using (var file = File.OpenWrite(fileName))
                    await file.WriteAsync(await uri.GetBytesAsync());

                var fi = new FileInfo(fileName);
                Console.WriteLine($"Fetched {fi.Length} bytes from {uri} into {fileName} ({fi.FullName})");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Failed to fetch from {uri} into {fileName}: {ex.Message}");
                if (!string.IsNullOrEmpty(backupLocalPath))
                {
                    Console.WriteLine($"Using backup file {backupLocalPath}");
                    return backupLocalPath;
                }
            }

            return fileName;
        }
    }
}
