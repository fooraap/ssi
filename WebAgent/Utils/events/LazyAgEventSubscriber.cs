﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;

namespace WebAgent.Utils.events
{
    public class LazyAgEventSubscriber<T, TSub> : AgEventSubscriber<T> where TSub : AgEventSubscriber<T>
    {

        private readonly Func<IApplicationBuilder> _appFunc;

        public LazyAgEventSubscriber(Func<IApplicationBuilder> appFunc)
        {
            _appFunc = appFunc;
        }

        public override async Task ProcessAsync(T item)
        {
            //await Task.Yield();
            var app = _appFunc.Invoke();
            if (app == null)
                throw new InvalidOperationException("No app builder");
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                var subscriber = serviceScope.ServiceProvider.GetService<TSub>();
                if (subscriber != null)
                {
                    await subscriber.ProcessAsync(item);
                }
            }
        }
    }
}
