﻿using System;
using System.Threading.Tasks;

namespace WebAgent.Utils.events
{
    public abstract class AgEventSubscriber<T> 
    {

        public Action<T> OnNext => new Action<T>(async (item) => await ProcessAsync(item));

        public abstract Task ProcessAsync(T item);

    }
}
