﻿using Hyperledger.Aries.Contracts;
using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using WebAgent.Utils.events;

namespace WebAgent.Utils.credpipe
{
    public class AgEventHandler : IDisposable
    {

        private readonly IEventAggregator _events;
        private IList<IDisposable> _subscribers;

        public AgEventHandler(IEventAggregator aggregator)
        {
            _subscribers = new List<IDisposable>();
            _events = aggregator;
        }

        public void Dispose()
        {
            foreach (var sub in _subscribers)
                sub.Dispose();
        }

        public IApplicationBuilder App { get; set; }

        public AgEventHandler LazySubscribe<TEvent, TSub>() where TSub : AgEventSubscriber<TEvent>
        {
            Subscribe<TEvent>(new LazyAgEventSubscriber<TEvent, TSub>(() => App));
            return this;
        }

        public AgEventHandler Subscribe<T>(AgEventSubscriber<T> sub)
        {
            var subscriber = _events.GetEventByType<T>();
            if (subscriber != null)
            {
                _subscribers.Add(subscriber.Subscribe(sub.OnNext));
            }
            return this;
        }
    }
}
