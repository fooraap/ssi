﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using WebAgent.Utils.credpipe;

namespace WebAgent.Utils.storage
{
    public class CredentialIssuancePipelineStore : IStore<KeyedPayload<Guid>, Guid>
    {

        private readonly ItemDbContext _context;

        public CredentialIssuancePipelineStore(ItemDbContext context)
        {
            _context = context;
        }
       
        public async Task<KeyedPayload<Guid>> LoadAsync(Guid key)
        {
            return await _context.Items.Where(item => item.Key == key)
                                       .FirstOrDefaultAsync();
        }

        public async Task RemoveAsync(Guid key)
        {
            var existing = await LoadAsync(key);
            if (existing != null)
            {
                _context.Items.Remove(existing);
                await _context.SaveChangesAsync();
            }
        }

        public async Task StoreAsync(KeyedPayload<Guid> item)
        {
            await _context.Items.AddAsync(item);
            await _context.SaveChangesAsync();
        }
    }
}
