﻿using Hyperledger.Aries.Features.DidExchange;
using Hyperledger.Aries.Models.Records;
using Newtonsoft.Json;
using System;

namespace WebAgent.Utils.credpipe
{
    /// <summary>
    /// Contains data and logic that represents a credential issuance pipeline. Such a pipeline consists of
    /// accumulating the necessary information (connection, credential definition, data) and the issuance 
    /// of a credential. It is assumed that the pipeline is to be persisted in between calls - especially
    /// when used in conjunction with a stateless interface such as REST or OpenAPI. 
    /// </summary>
    public class CredentialIssuancePipeline : IKeyedItem<Guid>
    {

        public CredentialIssuancePipeline()
        {

        }

        /// <summary>
        /// Unique key by which the specific pipeline can be stored and retrieved.
        /// </summary>
        public Guid Key { get; set; }

        /// <summary>
        /// Additional client id
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// The Credential Definition that is to be used when issuing 
        /// </summary>
        public DefinitionRecord CredentialDefinition { get; set; }

        /// <summary>
        /// The schema definition. This is optional and is only set when <see cref="LoadFromDefinitionId(string)"/>
        /// is used.
        /// </summary>
        public SchemaRecord SchemaDefinition { get; set; }

        /// <summary>
        /// The connection to which the credential is to be issued
        /// </summary>
        public ConnectionRecord Connection { get; set; }

        /// <summary>
        /// The credential source data. This data should not be persisted and should be short-lived
        /// </summary>
        [JsonIgnore]
        public object CredentialSourceData { get; set; }

        /// <summary>
        /// Creates a list of CredentialPreviewAttribute instances from the CredentialSourceData. This
        /// effectively creates the link between some form of (personal) information and a credential
        /// </summary>
        protected virtual string CredentialAttributes
        {
            get
            {
                return CredentialSourceData?.AsNameValueText();
            }
        }

        /// <summary>
        /// Returns true wh
        /// </summary>
        [JsonIgnore]
        public bool CanIssue => Connection != null && CredentialSourceData != null && CredentialDefinition != null;

      
    }
}
