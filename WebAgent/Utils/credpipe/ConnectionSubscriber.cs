﻿using Hyperledger.Aries.Agents;
using Hyperledger.Aries.Features.DidExchange;
using Hyperledger.Aries.Models.Events;
using Hyperledger.Aries.Storage;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Threading.Tasks;
using WebAgent.Hubs;
using WebAgent.Utils.events;

namespace WebAgent.Utils.credpipe
{
    public class ConnectionSubscriber : AgEventSubscriber<ServiceMessageProcessingEvent>
    {

        private readonly IAgentProvider _agentProvider;
        private readonly IWalletRecordService _recordService;
        private readonly CredentialIssuancePipelineManager _pipeManager;
        private readonly OidcService _oidc;
        private readonly IHubContext<ConnectionHub> _hubContext;

        public ConnectionSubscriber(CredentialIssuancePipelineManager pipeManager,
                                    IAgentProvider agentContextProvider,
                                    IWalletRecordService recordService,
                                    OidcService oidc,
                                    IHubContext<ConnectionHub> hubContext)
        {
            _pipeManager = pipeManager;
            _recordService = recordService;
            _agentProvider = agentContextProvider;
            _oidc = oidc;
            _hubContext = hubContext;
        }

        public override async Task ProcessAsync(ServiceMessageProcessingEvent item)
        {
            try
            {
                //await Task.Yield();
                var context = await _agentProvider.GetContextAsync();
                var connection = await _recordService.GetAsync<ConnectionRecord>(context.Wallet, item.RecordId);
                
                if (connection == null)
                    return;

                var pipe = await _pipeManager.RestoreAsync(connection.Id);

                if (pipe == null)
                    return; // Pipe does not exist for connection
                
                var clientProxy = _hubContext.Clients.Client(pipe.ClientId);
                await clientProxy.SendAsync("ConnectionConfirmed", await _oidc.Initiate(pipe.Key.ToString()));
            }
            catch (Exception ex)
            {
                return;
            }
        }
    }
}
