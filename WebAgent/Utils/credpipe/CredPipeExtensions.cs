﻿using Hyperledger.Aries.Models.Events;
using Microsoft.AspNetCore.Builder;

namespace WebAgent.Utils.credpipe
{
    public static class CredPipeExtensions
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="appBuilder">App.</param>
        public static void InitializeAgEventHandler(this IApplicationBuilder appBuilder)
        {
            var handler = appBuilder.ApplicationServices.GetService(typeof(AgEventHandler)) as AgEventHandler;
            handler.App = appBuilder;
        }
         

    }
}
