﻿using Hyperledger.Aries.Agents;
using Hyperledger.Aries.Configuration;
using Hyperledger.Aries.Features.DidExchange;
using Hyperledger.Aries.Features.IssueCredential;
using Hyperledger.Aries.Models.Records;
using Hyperledger.Aries.Storage;
using Hyperledger.Aries.Extensions;
using Hyperledger.Indy.AnonCredsApi;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAgent.Models;

namespace WebAgent.Utils.credpipe
{


    public class CredentialIssuancePipelineManager
    {

        private readonly IAgentProvider _agentContextProvider;
        private readonly IWalletRecordService _recordService;
        private readonly ISchemaService _schemaService;
        private readonly ICredentialService _credentialService;
        private readonly IProvisioningService _provisionService;
        private readonly IConnectionService _connectionService;
        private readonly IMessageService _messageService;
        private readonly IStore<KeyedPayload<Guid>, Guid> _store;
        private readonly IEntityEncoder<CredentialIssuancePipeline> _encoder;


        public CredentialIssuancePipelineManager(IAgentProvider agentContextProvider,
                                                 IWalletRecordService recordService,
                                                 ISchemaService schemaService,
                                                 ICredentialService credentialService,
                                                 IProvisioningService provisioningService,
                                                 IConnectionService connectionService,
                                                 IMessageService messageService,
                                                 IStore<KeyedPayload<Guid>, Guid> store,
                                                 IEntityEncoder<CredentialIssuancePipeline> encoder)
        {
            _schemaService = schemaService;
            _recordService = recordService;
            _agentContextProvider = agentContextProvider;
            _credentialService = credentialService;
            _provisionService = provisioningService;
            _connectionService = connectionService;
            _messageService = messageService;
            _store = store;
            _encoder = encoder;
        }


        public async Task<CredentialIssuancePipeline> RestoreAsync(string key)
        {
            if (Guid.TryParse(key, out Guid guid))
            {
                var entity = await _store.LoadAsync(guid);
                if (entity != null)
                    return await _encoder.DecodeAsync(entity.Payload);
            }
            return null;
        }

        public async Task StoreAsync(CredentialIssuancePipeline cip)
        {
            if (cip == null)
                throw new ArgumentNullException(nameof(cip));

            if (cip.Key == Guid.Empty)
                throw new ArgumentOutOfRangeException(nameof(cip.Key));

            await ForgetAsync(cip);

            var item = new KeyedPayload<Guid>()
            {
                Key = cip.Key,
                Payload = await _encoder.EncodeAsync(cip)
            };

            await _store.StoreAsync(item);
            cip.Key = item.Key;
        }

        public async Task ForgetAsync(CredentialIssuancePipeline cip)
        {
            if (cip == null)
                throw new ArgumentNullException(nameof(cip));

            await _store.RemoveAsync(cip.Key);
        }


        /// <summary>
        /// Issues the credential based on the information present in this pipeline instance
        /// </summary>
        /// <returns></returns>
        public async Task IssueCredentialAsync(CredentialIssuancePipeline cip)
        {
            if (cip == null)
                throw new ArgumentNullException(nameof(cip));

            if (!(cip.CanIssue))
                throw new InvalidOperationException("CIP not valid for issuance");

            var context = await _agentContextProvider.GetContextAsync();
            var issuer = await _provisionService.GetProvisioningAsync(context.Wallet);
            var connection = await _connectionService.GetAsync(context, cip.Connection.Id);
            var values = JsonConvert.DeserializeObject<List<CredentialPreviewAttribute>>(cip.CredentialSourceData.AsNameValueText());

            foreach (CredentialPreviewAttribute attr in values.Where(v => string.IsNullOrEmpty(v.MimeType)))
            {
                attr.MimeType = CredentialMimeTypes.ApplicationJsonMimeType;
            }

            var (offer, _) = await _credentialService.CreateOfferAsync(context, new OfferConfiguration
            {
                CredentialDefinitionId = cip.CredentialDefinition.Id,
                IssuerDid = issuer.IssuerDid,
                CredentialAttributeValues = values,
            });
            await _messageService.SendAsync(context, offer, connection);

        }


        /// <summary>
        /// Attempts to loads a credential definition form the ledger. If found, both credential definition
        /// and schema definition will be set and optionally added to the wallet
        /// </summary>
        /// <param name="definitionId">Qualified credential definition id e.g. D5HRDGDSppwraBqRgSgEzb:3:CL:6192:default</param>
        /// <param name="addToWallet">Flag indicating adding obtained data to the wallet or not</param>
        /// <returns></returns>
        public async Task<CredentialIssuancePipeline> CreateFromDefinitionIdAsync(string definitionId)
        {
            var context = await _agentContextProvider.GetContextAsync();
            var definition = await _recordService.GetAsync<DefinitionRecord>(context.Wallet, definitionId);
            if (definition == null)
                return null;

            return new CredentialIssuancePipeline()
            {
                CredentialDefinition = definition,
            };
        }
    }
}
