﻿using Microsoft.EntityFrameworkCore;
using System;

namespace WebAgent.Utils.credpipe
{
    public class ItemDbContext : KeyedPayloadDbContext<KeyedPayload<Guid>, Guid>
    {

        //public ItemDbContext() :
        //     base(new DbContextOptionsBuilder<KeyedPayloadDbContext<KeyedPayload<Guid>, Guid>>().UseNpgsql("User Id=foo; password=bar; Host=localhost; Database=foobar").Options)
        //{

        //}

        public ItemDbContext(DbContextOptions<ItemDbContext> options) : base(options)
        {
            this.Database.Migrate();
        }

    }
}
