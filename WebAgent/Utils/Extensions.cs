﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace WebAgent.Utils
{
    public static class Extensions
    {
        public static string TimeAgo(this DateTime dateTime)
        {
            string result;
            var timeSpan = DateTime.Now.Subtract(dateTime);

            if (timeSpan <= TimeSpan.FromSeconds(15))
            {
                result = "Just now";
            }
            else if (timeSpan <= TimeSpan.FromSeconds(60))
            {
                result = $"{timeSpan.Seconds} seconds ago";
            }
            else if (timeSpan <= TimeSpan.FromMinutes(60))
            {
                result = timeSpan.Minutes > 1 ? $"{timeSpan.Minutes} minutes ago" : "About a minute ago";
            }
            else if (timeSpan <= TimeSpan.FromHours(24))
            {
                result = timeSpan.Hours > 1 ? $"{timeSpan.Hours} hours ago" : "About an hour ago";
            }
            else if (timeSpan <= TimeSpan.FromDays(30))
            {
                result = timeSpan.Days > 1 ? $"{timeSpan.Days} days ago" : "Yesterday";
            }
            else if (timeSpan <= TimeSpan.FromDays(365))
            {
                result = timeSpan.Days > 30
                    ? $"{timeSpan.Days / 30} months ago"
                    : "About a month ago";
            }
            else
            {
                result = timeSpan.Days > 365
                    ? $"{timeSpan.Days / 365} years ago"
                    : "About a year ago";
            }

            return result;
        }


        public static string GetSplitPart(this string input, char splitChar, int part)
        {
            if (string.IsNullOrEmpty(input))
                return input;

            var parts = input.Split(splitChar, StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length == 0 || parts.Length <= part)
                return null;

            return parts[part];
        }


        public static string AsNameValueText(this object source)
        {
            if (source == null)
                return null;

            var fields = source.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance)
                                         .Where(p => p.GetCustomAttribute<ExcludeNameValueEntryAttribute>() == null)
                                         .Select(p => $"{{ 'name': '{p.Name}', 'value': '{p.GetValue(source)}' }}");
            var sb = new StringBuilder("[");
            foreach (var field in fields)
            {
                sb.Append(field);
                sb.Append(",");
            }
            sb.Remove(sb.Length - 1, 1);
            return sb.Append("]").ToString();
        }
    }
}
