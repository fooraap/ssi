﻿namespace WebAgent.Utils
{
    public class OidcConfiguration
    {

        public static string SectionName = "OidcClientConfiguration";

        public string HostUrl { get; set; }

        public string IdpBaseUrl { get; set; }

        public string IdpMetadataUrl { get; set; }



    }
}
