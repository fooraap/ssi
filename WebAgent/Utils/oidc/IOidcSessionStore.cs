﻿using System.Threading.Tasks;
using WebAgent.Utils.oidc;

namespace WebAgent.Utils
{
    public interface IOidcSessionStore
    {

        Task<AuthorizeSession> GetSession(string sessionkey);

        Task<AuthorizeSession> CreateSession(AuthorizeSession state); 

    }
}
