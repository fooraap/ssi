﻿using IdentityModel.OidcClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using WebAgent.Utils.oidc;

namespace WebAgent.Utils
{
    public class DbOidcSessionStore : IOidcSessionStore
    {

        private readonly OidcDbContext _context;

        public DbOidcSessionStore(OidcDbContext context)
        {
            _context = context;
        }

        public async Task<AuthorizeSession> CreateSession(AuthorizeSession session)
        {
            if (session == null)
                return null;

            if (string.IsNullOrEmpty(session.State))
                session.State = Guid.NewGuid().ToString();

            var entity = await _context.Sessions.AddAsync(session);
            await _context.SaveChangesAsync();
            return entity.Entity;
        }

        public async Task<AuthorizeSession> GetSession(string sessionkey)
        {
            return await _context.Sessions.Where(a => a.State == sessionkey)
                                          .FirstOrDefaultAsync();
        }
    }
}
