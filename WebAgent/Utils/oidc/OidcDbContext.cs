﻿using Microsoft.EntityFrameworkCore;

namespace WebAgent.Utils.oidc
{
    public class OidcDbContext : DbContext
    {

        //public OidcDbContext() :
        //     base(new DbContextOptionsBuilder<OidcDbContext>().UseNpgsql("User Id=foo; password=bar; Host=localhost; Database=foobar").Options)
        //{

        //}

        public OidcDbContext(DbContextOptions<OidcDbContext> options) : base(options)
        {
            this.Database.Migrate();
        }

        public DbSet<AuthorizeSession> Sessions { get; set; }

    }
}
