﻿namespace WebAgent.Models
{
    public class OidcRegistrationResponse
    {

        public string client_id { get; set; }

        public string client_secret { get; set; }

    }
}
