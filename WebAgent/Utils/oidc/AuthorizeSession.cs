﻿using IdentityModel.OidcClient;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebAgent.Utils.oidc
{
    public class AuthorizeSession : AuthorizeState
    {

        public AuthorizeSession()
        {

        }

        public AuthorizeSession(AuthorizeState state)
        {
            this.CodeVerifier = state.CodeVerifier;
            this.Nonce = state.Nonce;
            this.RedirectUri = state.RedirectUri;
            this.StartUrl = state.StartUrl;
            this.State = state.State;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string ClientId { get; set; }

        public string ClientSecret { get; set; }

        public string CustomState { get; set; }

    }
}
