﻿using Flurl.Http;
using IdentityModel.OidcClient;
using Microsoft.Extensions.Options;
using System;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using WebAgent.Models;
using WebAgent.Utils.oidc;

namespace WebAgent.Utils
{
    /// <summary>
    /// Basic OIDC client. Current implementation is pretty much customized for interaction with
    /// e-contract.be. 
    /// TODO: create some kind of storage to keep track of pending auth requests between calls
    /// </summary>
    public class OidcService
    {
        private readonly OidcConfiguration _config;
        private readonly IOidcSessionStore _sessionStore;

        public OidcService(IOptions<OidcConfiguration> config, IOidcSessionStore sessionStore)
        {
            _config = config.Value;
            _sessionStore = sessionStore;
        }

        private OidcClient CreateClient(string clientId, string clientSecret)
        {
            var options = new OidcClientOptions
            {
                Authority = $"{_config.IdpBaseUrl}",
                ClientId = clientId,
                ClientSecret = clientSecret,
                RedirectUri = $"{_config.HostUrl}/Credentials/ProcessEidCred",
                Scope = "openid profile address",
            };

            return new OidcClient(options);
        }

        public async Task<string> Initiate(string customState = null)
        {
            var regResponse = await(await $"{_config.IdpBaseUrl}/register".PostJsonAsync(new { redirect_uris = new[] { $"{_config.HostUrl}/Credentials/ProcessEidCred" } })).GetJsonAsync<OidcRegistrationResponse>();

            var client = CreateClient(regResponse.client_id, regResponse.client_secret);

            var state = await _sessionStore.CreateSession(new AuthorizeSession(await client.PrepareLoginAsync())
            {
                ClientId = regResponse.client_id,
                ClientSecret = regResponse.client_secret,
                CustomState = customState,
            });

            return state.StartUrl;
        }


        public async Task<(T, AuthorizeSession, ClaimsPrincipal)> ProcessAndGetUser<T>(string code, string state)
        {
            var session = await _sessionStore.GetSession(state);

            if (session == null)
                throw new InvalidOperationException("Invalid session key");

            var client = CreateClient(session.ClientId, session.ClientSecret);

            var result = await client.ProcessResponseAsync($"code={code}&state={state}", session);

            return (await $"{_config.IdpBaseUrl}/userinfo".WithOAuthBearerToken(result.AccessToken)
                                                          .GetJsonAsync<T>(), session, result.User);
        }
    }
}
