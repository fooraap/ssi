﻿using Newtonsoft.Json;
using System.Linq;
using System.Reflection;
using System.Security.Claims;

namespace WebAgent.Utils.oidc
{
    public static class ClaimsPrincipalExtensions
    {

        public static T Map<T>(this ClaimsPrincipal principal, T existing = null) where T : class, new()
        {
            var target = existing ?? new T();

            foreach (var property in typeof(T).GetProperties().Select(p => new { prop = p, attr = p.GetCustomAttribute<FromClaimAttribute>() }))
            {
                if (property.attr == null)
                    continue;

                var claim = principal.Claims.FirstOrDefault(c => c.Type == (string.IsNullOrEmpty(property.attr.Claim) ? property.prop.Name : property.attr.Claim));
                if (claim == null)
                    continue;

                if (!property.attr.Recursive)
                {
                    property.prop.SetValue(target, claim.Value);
                }
                else
                {
                    property.prop.SetValue(target, JsonConvert.DeserializeObject(claim.Value, property.prop.PropertyType));
                }
            }

            return target;
        }
    }
}
