﻿using Hyperledger.Aries.Agents;
using Hyperledger.Aries.Features.DidExchange;
using Hyperledger.Aries.Storage;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;
using WebAgent.Configuration;
using WebAgent.Utils.credpipe;

namespace WebAgent.Hubs
{
    public class ConnectionHub : Hub
    {

        private readonly CredentialIssuancePipelineManager _pipelineManager;
        private readonly IAgentProvider _agentProvider;
        private readonly IWalletRecordService _recordService;
        private readonly IssuancePipelineConfiguration _options;
        private readonly ILogger _log;

        public ConnectionHub(CredentialIssuancePipelineManager pipelineManager,
                             IAgentProvider agentProvider,
                             IWalletRecordService recordService,
                             IOptions<IssuancePipelineConfiguration> options,
                             ILogger<ConnectionHub> log)
        {
            _pipelineManager = pipelineManager;
            _recordService = recordService;
            _agentProvider = agentProvider;
            _options = options.Value;
            _log = log;
        }

        public async Task SetConnectionId(string userId, string connectionId)
        {

            _log.LogInformation($"Connection tuple {userId} => {connectionId}. Initiating issuance pipeline for {_options.SchemaDefinition}.");

            var newPipe = await _pipelineManager.CreateFromDefinitionIdAsync(_options.SchemaDefinition);  //("D5HRDGDSppwraBqRgSgEzb:3:CL:6540:be_eid"); // TODO: move to config.

            if (newPipe == null)
            {
                _log.LogError("No pipeline was constructed. Is your credential definition correct and present in your wallet?");
                return;
            }    

            var context = await _agentProvider.GetContextAsync();
            
            newPipe.Connection = await _recordService.GetAsync<ConnectionRecord>(context.Wallet, connectionId);
            newPipe.ClientId = userId;
            newPipe.Key = Guid.Parse(newPipe.Connection.Id);

            await _pipelineManager.StoreAsync(newPipe);
        }


    }
}
