﻿
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System;
using System.IO;
using System.Threading.Tasks;
using WebAgent.Logging;
using WebAgent.Logging.LogProviders;

namespace WebAgent
{
    public class Program
    {

        private static readonly StreamWriter error = new StreamWriter(new InterceptedMemoryStream());

        public static Task Main(string[] args)
        {
            Console.SetError(error);

            var host = CreateHostBuilder(args).Build();

            return host.RunAsync();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((context, config) =>
                {
                    config.AddEnvironmentVariables();
                })
                .ConfigureWebHostDefaults(webBuilder => webBuilder.UseStartup<Startup>());
    }
}
