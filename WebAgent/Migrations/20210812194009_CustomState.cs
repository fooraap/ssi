﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebAgent.Migrations
{
    public partial class CustomState : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CustomState",
                table: "Sessions",
                type: "text",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CustomState",
                table: "Sessions");
        }
    }
}
