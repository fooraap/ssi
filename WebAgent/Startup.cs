using System;
using System.IO;
using FluentValidation.AspNetCore;
using Hyperledger.Aries.Contracts;
using Hyperledger.Aries.Features.IssueCredential;
using Hyperledger.Aries.Models.Events;
using Hyperledger.Aries.Storage;
using Hyperledger.Indy.Utils;
using Jdenticon.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using WebAgent.Configuration;
using WebAgent.Hubs;
using WebAgent.Messages;
using WebAgent.Models;
using WebAgent.Protocols.BasicMessage;
using WebAgent.Utils;
using WebAgent.Utils.credpipe;
using WebAgent.Utils.oidc;
using WebAgent.Utils.storage;

namespace WebAgent
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Aries Open Api Requires Fluent Validation
            services
                .AddMvc()
                .AddFluentValidation()
                .AddAriesOpenApi(a => a.UseSwaggerUi = true);

            services.AddLogging(logger =>
            {
                logger.AddSerilog(new LoggerConfiguration().MinimumLevel.Verbose()
                                                           .MinimumLevel.Override("Microsoft", Serilog.Events.LogEventLevel.Warning)
                                                           .WriteTo.Console()
                                                           .CreateLogger(), true);
                //logger.A
            });

            // Register agent framework dependency services and handlers
            services.AddAriesFramework(builder =>
            {
                builder.RegisterAgent<SimpleWebAgent>(c =>
                {
                    var agentConfig = new AgentConfiguration();
                    Configuration.GetSection(AgentConfiguration.SectionName).Bind(agentConfig);

                    Console.WriteLine(agentConfig.ToString());


                    c.AgentName = agentConfig.Name;
                    c.EndpointUri = agentConfig.EndpointUrl; 
                    c.WalletConfiguration = new WalletConfiguration
                    {
                        Id = agentConfig.WalletConfiguration,
                        StorageConfiguration = new WalletConfiguration.WalletStorageConfiguration()
                        {
                            Path = agentConfig.WalletPath
                        }
                    };
                    c.WalletCredentials = new WalletCredentials { Key = agentConfig.WalletKey };
                    c.AgentKeySeed = agentConfig.SeedKey; 
                    c.IssuerKeySeed = agentConfig.SeedKey; 
                    c.GenesisFilename = WebExtensions.LeechAndStoreAsync(agentConfig.GenesisUri, agentConfig.LocalGenesisFile).GetAwaiter().GetResult(); 
                    c.PoolName = agentConfig.PoolName;
                    c.AutoRespondCredentialOffer = true;
                    c.AutoRespondCredentialRequest = true;
                });
            });

            services.Configure<OidcConfiguration>(Configuration.GetSection(OidcConfiguration.SectionName));
            services.Configure<IssuancePipelineConfiguration>(Configuration.GetSection(IssuancePipelineConfiguration.SectionName));
            services.Configure<AgentConfiguration>(Configuration.GetSection(AgentConfiguration.SectionName));

            // Register custom handlers with DI pipeline
            services.AddSingleton<BasicMessageHandler>();
            services.AddSingleton<TrustPingMessageHandler>();

            // OIDC session handling
            services.AddDbContext<OidcDbContext>(builder =>
            {
                builder.UseNpgsql(Configuration.GetConnectionString("oidcsessiondb"));
            });
            // VC issuance pipeline item store
            services.AddDbContext<ItemDbContext>(builder =>
            {
                builder.UseNpgsql(Configuration.GetConnectionString("oidcsessiondb"));
            });

            services.AddTransient<IOidcSessionStore, DbOidcSessionStore>();
            services.AddTransient<OidcService>();
           
            services.AddTransient<CredentialIssuancePipelineManager>();
            services.AddTransient<IStore<KeyedPayload<Guid>, Guid>, CredentialIssuancePipelineStore>();
            services.AddTransient<IEntityEncoder<CredentialIssuancePipeline>, EntityJsonEncoder<CredentialIssuancePipeline>>();

            services.AddTransient<ConnectionSubscriber>();
            services.AddSingleton<AgEventHandler>((provider) => new AgEventHandler(provider.GetRequiredService<IEventAggregator>())
                                                                                           .LazySubscribe<ServiceMessageProcessingEvent, ConnectionSubscriber>());

            //services.AddSingleton<IUserIdProvider, ConnectionIdUserIdProvider>();
            services.AddSignalR();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            app.UsePathBase("/eidagent");

            var provider = new FileExtensionContentTypeProvider();

            provider.Mappings[".txn"] = "application/json";


            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(env.WebRootPath),
                ContentTypeProvider = provider,
            });

            // Register agent middleware
            app.UseAriesFramework();

            // Configure OpenApi
            app.UseAriesOpenApi();

            // fun identicons
            app.UseJdenticon();
            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapHub<ConnectionHub>("/connectionHub");
            });

            app.InitializeAgEventHandler();
        }
    }
}
