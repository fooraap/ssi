﻿using System;
using System.IO;

namespace WebAgent
{
    internal class InterceptedMemoryStream : MemoryStream
    {

        public override void Write(byte[] buffer, int offset, int count)
        {
            base.Write(buffer, offset, count);
        }

        public override void Write(ReadOnlySpan<byte> source)
        {
            base.Write(source);
        }

        public override void WriteByte(byte value)
        {
            base.WriteByte(value);
        }

    }
}