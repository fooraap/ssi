﻿"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("/eidagent/connectionHub").build();
var connectionId = '';


connection.on("ConnectionConfirmed", function (redirectUrl) {
    window.location.replace(redirectUrl);
});

connection.start().then(function () {
    sendConnectionId();
}).catch(function (err) {
    console.error(err.toString());
});

function setConnectionParameters(id) {
    connectionId = id;
}

function sendConnectionId() {
    if (connectionId) {
        connection.invoke("SetConnectionId", connection.connectionId, connectionId).catch(function (err) {
            return console.error(err.toString());
        });
    }
}



//document.getElementById("sendButton").addEventListener("click", function (event) {
//    var user = document.getElementById("userInput").value;
//    var message = document.getElementById("messageInput").value;
//    connection.invoke("SendMessage", user, message).catch(function (err) {
//        return console.error(err.toString());
//    });
//    event.preventDefault();
//});