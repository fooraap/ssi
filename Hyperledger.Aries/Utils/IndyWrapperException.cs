﻿using System;
using WebAgent.Utils;

namespace Hyperledger.Aries.Utils
{
    public class IndyWrapperException : Exception
    {

        private const string DefaultMessage = "Indy exception. See LastIndyError and inner exception for more details.";


        public IndyWrapperException(Exception inner) : base(DefaultMessage, inner)
        {
            string error = null;
            LoggingMethods.indy_get_current_error(ref error);
            LastIndyError = error;
        }

        public string LastIndyError { get; private set; }

        public override string ToString()
        {
            return $"Last SDK error: {LastIndyError}{Environment.NewLine}{base.ToString()}";
        }

    }
}
